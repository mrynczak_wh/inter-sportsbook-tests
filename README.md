### INSTALLATION

This project is using _yarn_ package manager.
If you do not have _yarn_ manager you need to install that before next steps. Please do following command:

        npm install -g yarn

Once you have _yarn_ installed you can install all the dependencies. To do that please type into terminal:

        yarn install


### TESTS RUNNING

There are 2 modes of running the tests:
* debug mode - with open preview of tests execution, one spec file is running at the moment

        yarn cy open

* execution mode - there are running all of the provided in CLI specs, tests are running in the background

        yarn cy run

For now there are options to run the tests for following options:
* in the Spanish jurisdiction
* in the Italian jurisdiction
* in the PP1 environment
* in the Production environment
* in the mobile view
* in the desktop view
* there are plans to add possibility of running tests for .COM (UK) jurisdiction and in the tablet view

All of the options are listed below:

```
  --devices, -d  Specify a space separated list of devices to emulate
                    [choices: "mobile", "desktop"] [default: "desktop"]
                    
  --jurisdiction, -j Specify a jurisdiction to run tests against
                    [choices: "es", "it"] [default: "es"]
                    
  --env, -e      Specify environment to run tests against
                    [choices: "prod", "pp1"] [default: "prod"]
                    
  --browsers, -b Specify a space separated list of browsers to run tests in
                    [choices: "chrome", "electron", "firefox", "edge"] [default: "electron"]
                    
  --spec, -s     Specify path to spec file(s) to run, may be passed as a space separated list
  
  --reporter, -r Specify reporter for test results
                    [choices: "spec", "mochawesome"] [default: "spec"]
```

Example of executing the tests for Spanish jurisdiction in production in the mobile view:

    yarn cy run -j es -e prod -d mobile

Example of running the tests in interactive mode for Italian jurisdiction in PP1 and desktop view:

    yarn cy run -j it -e pp1 -d desktop

Providing default values into CLI is optional (like `-d desktop`).
