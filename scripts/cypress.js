const cy = require('cypress');
const yargs = require('yargs');
const chalk = require('chalk');
const { generateMochawesomeReport, partialReportsDir, trashAssetsBeforeRun } = require('../utils/mochawesomeReport');

const MOBILE = 'mobile';
const DESKTOP = 'desktop';
const TABLET = 'tablet';

const { argv } = yargs
    .env('CY')
    .command('open', 'Open interactive Cypress Test Runner')
    .command('run', 'Run Cypress tests')
    .demandCommand(1, 'Please specify a command')
    .options({
        devices: {
            alias: 'd',
            type: 'array',
            choices: [MOBILE, TABLET, DESKTOP],
            default: DESKTOP,
            desc: 'Specify devices to emulate',
        },
        env: {
            alias: 'e',
            choices: ['prod', 'pp1'],
            default: 'prod',
            desc: 'Specify environment to run tests against',
        },
        browsers: {
            alias: 'b',
            type: 'array',
            choices: ['chrome', 'electron', 'firefox', 'edge'],
            default: 'electron',
            desc: 'Specify browsers to run tests in',
        },
        spec: {
            alias: 's',
            desc: 'Specify spec file to run',
            type: 'array'
        },
        reporter: {
            alias: 'r',
            desc: 'Specify reporter of test results',
            choices: ['spec', 'mochawesome'],
            default: 'spec',
        },
        jurisdiction: {
            alias: 'j',
            choices: ['es', 'it'],
            default: 'es',
            desc: 'Select jurisdiction to run tests for',
        },
    })
    .help();

const { devices, env, browsers, spec, reporter, jurisdiction, _: command } = argv;

/* eslint-enable no-param-reassign */
const getBaseConfig = () => {
    // eslint-disable-next-line import/no-dynamic-require
    const baseConfig = require(`../config/cypress.${jurisdiction}.${env}.json`);
    return baseConfig;
};

const mochawesomeOpts = {
    html: false,
    consoleReporter: 'spec',
    reportDir: partialReportsDir,
    overwrite: false,
};

const setReporter = () => {
    const result = { reporter };
    if (reporter === 'mochawesome') {
        result.reporterOptions = mochawesomeOpts;
    }
    return result;
};

// Set Cypress.env variables
const getCypressEnv = (device, selectedEnv, domain, jurisdiction) => ({
    device,
    domain,
    jurisdiction,
    isMobile: device === MOBILE,
    isTablet: device === TABLET,
    isDesktop: device === DESKTOP,
    environment: selectedEnv,
    RETRIES: 1,
});

const setViewport = (width, height) => ({ viewportHeight: height, viewportWidth: width });

const getViewportForDevice = device => {
    switch (device) {
        case MOBILE:
            return setViewport(375, 812);
        case TABLET:
            return setViewport(1024, 1366);
        default:
            return setViewport(1440, 900);
    }
};

const getUserAgentForDevice = device => {
    switch (device) {
        case MOBILE:
            return 'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.83 Mobile Safari/537.36';
        case TABLET:
            return 'Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-T865) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.1 Chrome/71.0.3578.99 Safari/537.36';
        default:
            return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/90.0.4392.0 Safari/537.36';
    }
};

const getOptsCombinations = () => {
    if (command === 'open') {
        // if `cy open` is run, get only the first choices, ignore the rest
        return [{ browser: browsers[0], device: devices[0] }];
    } else {
        // generate all browser-device combinations
        return browsers.reduce((acc, browser) => [...acc, ...devices.map(device => ({ device, browser }))], []);
    }
};

const getCypressRunOptions = () => {
    const baseConfig = getBaseConfig();
    // generate an array of all configurations
    return getOptsCombinations().map(({ browser, device }) => {
        return {
            spec,
            browser,
            ...setReporter(),
            env: getCypressEnv(device, env, baseConfig.domain, jurisdiction),
            configFile: false,
            config: {
                ...baseConfig,
                ...getViewportForDevice(device),
                userAgent: getUserAgentForDevice(device),
                video: false,
                trashAssetsBeforeRuns: !(reporter === 'mochawesome'),
                defaultCommandTimeout: 10000,
                requestTimeout: 10000,
            },
        };
    });
};

const generateReport = () =>
    new Promise(resolve => {
        if (reporter === 'mochawesome') {
            resolve(generateMochawesomeReport());
        }
        resolve();
    });

const hasAnyTestFailed = results => results.some(result => result.totalFailed > 0 || result.failures > 0);

const shutdownGracefully = results =>
    generateReport()
        .then(
            () =>
                new Promise((resolve, reject) =>
                    hasAnyTestFailed(results)
                        ? reject(chalk.red.bold('\u{1F4A3} Some tests have failed... \u{1F62D}'))
                        : resolve(chalk.green.bold('\u{1F37E} Completed successfully'))
                )
        )
        .then(console.info);

function* getCypressRunResults(runOptionsArray) {
    while (runOptionsArray.length) {
        yield cy[command](runOptionsArray.shift());
    }
}

const runCypress = async () => {
    const results = [];
    // eslint-disable-next-line no-restricted-syntax
    for await (const result of getCypressRunResults(getCypressRunOptions())) {
        results.push(result);
    }
    return results;
};

const run = () =>
    trashAssetsBeforeRun()
        .then(runCypress)
        .then(shutdownGracefully)
        .catch(err => {
            console.error(err);
            process.exit(1);
        });

run();
