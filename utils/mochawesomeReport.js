/* eslint-disable no-console */
const fs = require('fs-extra');
const { merge: mergeReports } = require('mochawesome-merge');
const reportGenerator = require('mochawesome-report-generator');
const chalk = require('chalk');

const partialReportsDir = 'cypress/reports/tmp/';

const removePartialReports = () => {
    return fs
        .remove('cypress/reports/tmp/')
        .catch(error => console.log(chalk.red(`\u274C An error occurred while clearing partial reports\n${error}`)));
};

const trashAssetsBeforeRun = () =>
    removePartialReports()
        .then(() => fs.remove('cypress/screenshots/'))
        .catch(error => console.log(chalk.red(`\u274C An error occurred while clearing screenshots directory\n${error}`)));

const attachScreenshots = reportDir => {
    console.log(chalk.cyan('\u{1F4F7} Attaching screenshots...'));
    return fs
        .move('cypress/screenshots/', `${reportDir}/screenshots`)
        .then(() => console.log(chalk.green.bold('\u{1F4CE} Screenshots attached')))
        .catch(() => console.log(chalk.cyan('\u{1F3AF} No screenshots to attach')));
};

const generateMochawesomeReport = () => {
    const mergeOptions = {
        files: [`${partialReportsDir}*.json`],
    };
    const margeOptions = {
        reportDir: `cypress/reports/report-${new Date().toISOString()}`,
        charts: true,
        reportTitle: `Sportsbook Tests Report`,
        reportPageTitle: 'Sportsbook Spitfire Page Report',
    };
    return mergeReports(mergeOptions)
        .then(results => {
            console.log(chalk.cyan('\u{1F4C2} Generating report...'));
            return reportGenerator.create(results, margeOptions);
        })
        .then(() => {
            console.log(chalk.green.bold('\u2705 Report generated'));
            return attachScreenshots(margeOptions.reportDir);
        })
        .then(removePartialReports)
        .catch(error => console.log(chalk.red(`\u274C An error occurred while generating report\n${error}`)));
};

module.exports = {
    generateMochawesomeReport,
    trashAssetsBeforeRun,
    partialReportsDir,
};
