import * as BREADCRUMBS from '../../../selectors/breadcrumbsPage/breadcrumbs';
import * as MOBILE_CAROUSEL from '../../../selectors/mobileSportsCarousel/sportsCarousel';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';
import * as DAILY_LIST from "../../../selectors/dailyListPage/dailyListPageSelectors";
import * as EVENT_PAGE from "../../../selectors/eventPage/eventPage";
import * as HOMEPAGE from "../../../selectors/homepage/homepage";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('Navigation by breadcrumbs', () => {
    beforeEach(() => {
        Methods.openHomePage();
        Methods.navigateToFootballDailyListPage(envData.device);
    });


    it(`Home button in breadcrumbs navigates to Homepage - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(HOMEPAGE.PAGE_WRAPPER).should('be.visible');

        switch(envData.device) {
            case 'DESKTOP':
                Methods.selectPrematchEvent();
                cy.get(EVENT_PAGE.EVENT_PAGE_WRAPPER).should('be.visible');
                cy.get(EVENT_PAGE.MARKETS_MENU).should('be.visible');
                cy.get(EVENT_PAGE.MARKETS_CONTAINER).should('be.visible');
                cy.wait(1000);
                cy.get(BREADCRUMBS.HOME_BUTTON_DESKTOP).click({scrollBehavior: false});
                break;
            case 'TABLET':
                Methods.selectPrematchEvent();
                cy.get(EVENT_PAGE.EVENT_PAGE_WRAPPER).should('be.visible');
                cy.get(EVENT_PAGE.MARKETS_MENU).should('be.visible');
                cy.get(EVENT_PAGE.MARKETS_CONTAINER).should('be.visible');
                cy.wait(1000);
                cy.get(BREADCRUMBS.HOME_BUTTON_MOBILE).click({scrollBehavior: false});
                break;
            case 'MOBILE':
                Methods.navigateToFootballDailyListPage(envData.device);
                cy.get(DAILY_LIST.DAILY_LIST_WRAPPER).should('be.visible');
                cy.get(BREADCRUMBS.HOME_BUTTON_MOBILE).click({scrollBehavior: false});
                break;
        }
    });

    it(`Back button in breadcrumbs navigates to previous page - ${envData.jurisdiction} - ${envData.device}`, () => {
        switch(envData.device) {
            case 'MOBILE':
                cy.get(DAILY_LIST.DAILY_LIST_WRAPPER).should('be.visible');
                Methods.selectPrematchEvent();
                cy.get(EVENT_PAGE.EVENT_PAGE_WRAPPER).should('be.visible');
                cy.get(EVENT_PAGE.MARKETS_MENU).should('be.visible');
                cy.get(EVENT_PAGE.MARKETS_CONTAINER).should('be.visible');
                cy.wait(1000);
                cy.get(BREADCRUMBS.BACK_BUTTON).click({force: true});
                cy.get(DAILY_LIST.DAILY_LIST_WRAPPER).should('be.visible');
                Methods.shouldHaveTextAccordingJusrisdiction(DAILY_LIST.PAGE_TITLE, 'Lista Diaria', 'Calendario');
                break;
        }
    })
});

