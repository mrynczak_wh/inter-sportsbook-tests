import * as DAILY_LIST from '../../../selectors/dailyListPage/dailyListPageSelectors';
import * as Methods from '../../../support/methods';
import * as BETTING from '../../../selectors/betting/betting-options';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('Daily List Page - desktop', () => {

    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
        Methods.navigateToFootballDailyListPage(envData.device);
    });


    it(`Bet placement in football daily list page - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(DAILY_LIST.DAILY_LIST_WRAPPER).should('be.visible');
        cy.get(BETTING.SELECTION).eq(0).click({scrollBehavior: false});
        Methods.placeBetWithAssertion();
    })
});


