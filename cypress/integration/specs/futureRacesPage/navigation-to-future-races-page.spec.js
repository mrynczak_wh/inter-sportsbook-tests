import * as Methods from '../../../support/methods'
import * as HORSE_RACING from '../../../selectors/horseRacingPage/horseRacingPage'
import * as BREADCRUMB from '../../../selectors/breadcrumbsPage/breadcrumbs'
import * as EVENT_PAGE from '../../../selectors/eventPage/eventPage'
import * as BETSLIP from '../../../selectors/betslipWidget/betslip'
import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu'
import * as SUB_HEADER from '../../../selectors/subHeader/sub-header'
import * as MOBILE_TOOLBAR from '../../../selectors/mobileBottomToolbar/toolbar'

Cypress.on('uncaught:exception', () => {
  // returning false here prevents Cypress from failing the test
  return false
})

const envData = Methods.getEnvData()

const HEADING = 'Próximas carreras'

// Horse Racing page is not available for IT jurisdiction because of law regulations
if (envData.jurisdiction === 'ES') {

  describe('Navigation to Future Races page', () => {

    beforeEach(() => {
      Methods.openHomePage()
      Methods.navigateToHorseRacingPage(envData.device)
    })

    it(`Navigation to Future Races page from menu - ${envData.jurisdiction} - ${envData.device}`, () => {

      cy.get(HORSE_RACING.FUTURE_RACES_BUTTON)
        .click({ scrollBehavior: false })
      cy.get(EVENT_PAGE.EVENT_ERROR_MESSAGE)
        .should('not.exist')
      cy.get(HORSE_RACING.FUTURE_RACES_SECTION)
        .should('exist')
        .and('be.visible')
        .find('h1')
        .should('exist')
        .and('be.visible')
        .and('have.text', HEADING)

      switch (envData.device) {
        case 'MOBILE':
          cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('exist').and('be.visible')
          cy.get(BREADCRUMB.BACK_BUTTON).should('exist').and('be.visible')
          cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist').and('not.be.visible')
          cy.get(DSK_MENU.SPORTS_MENU).should('not.exist')
          cy.get(SUB_HEADER.SUB_HEADER).should('not.exist')
          break
        case 'TABLET':
          cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('exist').and('be.visible')
          cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist').and('not.be.visible')
          cy.get(SUB_HEADER.SUB_HEADER).should('not.exist')
          cy.get(BREADCRUMB.BACK_BUTTON).should('exist').and('be.visible')
          break
        case 'DESKTOP':
          cy.get(DSK_MENU.SPORTS_MENU).should('be.visible')
          cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist')
          cy.get(SUB_HEADER.SUB_HEADER).should('be.visible')
          cy.get(BREADCRUMB.BACK_BUTTON).should('not.exist')
          cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('exist').and('not.be.visible')
          break
      }
    })
  })
}