import * as Methods from '../../../support/methods'
import * as HORSE_RACING from '../../../selectors/horseRacingPage/horseRacingPage'
import * as BETTING from '../../../selectors/betting/betting-options'
import * as BETSLIP from '../../../selectors/betslipWidget/betslip'

Cypress.on('uncaught:exception', () => {
  // returning false here prevents Cypress from failing the test
  return false
})

const envData = Methods.getEnvData()

// Horse Racing page is not available for IT jurisdiction because of law regulations
if (envData.jurisdiction === 'ES') {

  describe('Bet placement in Future Races page', () => {
  
    beforeEach(() => {
        Methods.openHomePage()
        Methods.loginToSportsbook()
        Methods.navigateToHorseRacingPage(envData.device)
    })
  
    it(`Place single bet in Future Races page - ${envData.jurisdiction} - ${envData.device}`, () => {

      cy.get(HORSE_RACING.FUTURE_RACES_BUTTON)
        .click({scrollBehavior: false})
      cy.get(HORSE_RACING.RACE_NAV)
        .should('exist')
        .and('be.visible')
      cy.get(HORSE_RACING.RACE_GROUP_OPENED)
        .within(() => {
          cy.getRandomElement(HORSE_RACING.RACE_GROUP_ITEM)
            .click({ scrollBehavior: false, force: true })
      })
      cy.get(HORSE_RACING.RACE_CARD_CONTAINER)
        .should('exist')
        .and('be.visible')
        .within(() => {
          cy.getRandomElement(BETTING.BET_BUTTON)
            .click({ scrollBehavior: false, force: true })
      })
      Methods.putStakeInBetslip()
      cy.get(BETSLIP.INFO_LABEL)
        .should('not.exist')
      cy.get(BETSLIP.WARNING_MESSAGE)
        .should('not.exist')
      Methods.placeBet()
      Methods.assertBetIsPlaced()
    })
  })
}
