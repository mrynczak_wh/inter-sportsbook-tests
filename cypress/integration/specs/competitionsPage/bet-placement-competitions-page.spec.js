import * as COMPETITIONS_PAGE from '../../../selectors/competitionsPage/competitionsPage';
import * as Methods from '../../../support/methods';
import * as BETTING from '../../../selectors/betting/betting-options';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('Competitions Page', () => {

    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
        Methods.navigateToFootballCompetitionsPage(envData.device);
    });

    it(`Bet placement in football competition page - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(COMPETITIONS_PAGE.COMPETITIONS_PAGE_WRAPPER).should('be.visible');
        cy.get(COMPETITIONS_PAGE.SINGLE_COMP).first().click({scrollBehavior: false});
        cy.get(BETTING.SELECTION).eq(3).click({scrollBehavior: false});
        Methods.placeBetWithAssertion();
    })
});
