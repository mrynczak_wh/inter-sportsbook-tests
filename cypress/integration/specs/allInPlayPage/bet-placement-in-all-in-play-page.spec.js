import * as Methods from '../../../support/methods'
import * as IN_PLAY_PAGE from '../../../selectors/inPlayPage/inPlayPage'
import * as BETSLIP from '../../../selectors/betslipWidget/betslip'

Cypress.on('uncaught:exception', () => {
  // returning false here prevents Cypress from failing the test
  return false
});

const jurisdiction = Cypress.env('jurisdiction').toUpperCase();
const device = Cypress.env('device').toUpperCase();

describe('Bet placement in All In-Play page', () => {

  beforeEach(() => {
      Methods.openHomePage();
      Methods.loginToSportsbook();
      Methods.navigateToAllInPlayPage(device)
  });

  it(`Place single bet in All In-Play page - ${jurisdiction} - ${device}`, () => {
    cy.get(IN_PLAY_PAGE.CONTENT).should('exist').and('be.visible');
    cy.getRandomElement(IN_PLAY_PAGE.SELECTIONS_WRAPPER_ACTIVE)
      .within(() => {
        cy.getRandomElement(IN_PLAY_PAGE.SELECTION)
          .click({scrollBehavior: false, force: true})
      });
    Methods.putStakeInBetslip();
    cy.get(BETSLIP.INFO_LABEL).should('not.exist');
    cy.get(BETSLIP.WARNING_MESSAGE).should('not.exist');
    Methods.placeBet();
    Methods.assertBetIsPlaced()
  })
});
