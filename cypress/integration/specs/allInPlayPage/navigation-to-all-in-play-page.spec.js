import * as Methods from '../../../support/methods'
import * as Page from '../../../selectors/homepage/homepage'
import * as IN_PLAY_PAGE from '../../../selectors/inPlayPage/inPlayPage'
import * as BETSLIP from '../../../selectors/betslipWidget/betslip'
import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu'
import * as PRODUCT_HEADER from '../../../selectors/productHeader/header'
import * as SUB_HEADER from '../../../selectors/subHeader/sub-header'
import * as MOBILE_TOOLBAR from '../../../selectors/mobileBottomToolbar/toolbar'
import * as MOBILE_LAYOUT from '../../../selectors/mobileCommonLayout/layout'
import * as BREADCRUMBS from "../../../selectors/breadcrumbsPage/breadcrumbs";

Cypress.on('uncaught:exception', () => {
  // returning false here prevents Cypress from failing the test
  return false
});

const envData = Methods.getEnvData();

const HEADING = {
    'ES': 'Todos los deportes En Directo',
    'IT': 'Tutti Sports Live'
};

describe('Navigation to All In-Play page', () => {

    beforeEach(() => {
        Methods.openHomePage();
        Methods.navigateToAllInPlayPage(envData.device);
    });

    it(`Navigation to all in-play page from menu - ${envData.jurisdiction} - ${envData.device}`, () => {

        cy.get(Page.MAIN_CONTENT_AREA).contains('h1', HEADING[envData.jurisdiction]).should('exist').and('be.visible');
        cy.get(PRODUCT_HEADER.NAV_MENU).should('be.visible');
        cy.get(IN_PLAY_PAGE.ALL_IN_PLAY_CAROUSEL).should('be.visible');

        switch(envData.device) {
            case 'MOBILE':
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('exist').and('be.visible');
                cy.get(MOBILE_LAYOUT.BACK_BUTTON).should('exist').and('be.visible');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist').and('not.be.visible');
                cy.get(DSK_MENU.SPORTS_MENU).should('not.exist');
                cy.get(SUB_HEADER.SUB_HEADER).should('not.exist');
                break;
            case 'TABLET':
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('exist').and('be.visible');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist').and('not.be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                break;
            case 'DESKTOP':
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                cy.get(IN_PLAY_PAGE.ALL_IN_PLAY_CAROUSEL).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('exist').and('not.be.visible');
                break;
            default:

        }
    })
});
