import * as Methods from '../../../support/methods';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('HomePage', () => {

    beforeEach(() => {
        Methods.openHomePage();
    });


    it(`Page language - before login - ${envData.jurisdiction} - ${envData.device}`, () => {
        Methods.checkLangOfHomepage();

    });

    it(`Page language - user is logged in - ${envData.jurisdiction} - ${envData.device}`, () => {
        Methods.loginToSportsbook();
        Methods.checkLangOfHomepage();
        Methods.logoutInSportsbook();
    });

    it(`Page language - after logging out - ${envData.jurisdiction} - ${envData.device}`, () => {
        Methods.loginToSportsbook();
        Methods.logoutInSportsbook();
        Methods.checkLangOfHomepage();
    })
});
