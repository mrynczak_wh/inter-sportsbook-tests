import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as HOMEPAGE from '../../../selectors/homepage/homepage';
import * as TOP_BETS from '../../../selectors/topBetsWidget/top-bets-widget';
import * as BETSLIP from '../../../selectors/betslipWidget/betslip';
import * as SUB_HEADER from '../../../selectors/subHeader/sub-header';
import * as HEADER from '../../../selectors/productHeader/header';
import * as MOBILE_TOOLBAR from '../../../selectors/mobileBottomToolbar/toolbar';
import * as MOBILE_CAROUSEL from '../../../selectors/mobileSportsCarousel/sportsCarousel';
import * as MOBILE_COMMON from '../../../selectors/mobileCommonLayout/layout';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';
import * as BREADCRUMBS from "../../../selectors/breadcrumbsPage/breadcrumbs";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

describe('HomePage', () => {
    beforeEach(() => {
        Methods.openHomePage();
    });


    it(`Navigation to homepage by URL - ${envData.jurisdiction} - ${envData.device}`, () => {
        switch (envData.device) {
            case 'DESKTOP':
                cy.get(HOMEPAGE.PAGE_WRAPPER).should('be.visible');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.not.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('not.be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('not.exist');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                cy.get(MOB_MENU.SPORT_MENU).should('not.exist');
                break;
            case 'TABLET':
                cy.get(HOMEPAGE.PAGE_WRAPPER).should('be.visible');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('exist').and('not.be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                cy.get(MOB_MENU.SPORT_MENU).should('exist').and('not.be.visible');
                break;
            case 'MOBILE':
                cy.get(HOMEPAGE.PAGE_WRAPPER).should('be.visible');
                cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('not.be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOB_MENU.SPORT_MENU).should('not.be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('not.exist');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                cy.get(MOB_MENU.HAMBURGER).should('be.visible');
                break;
        }
    });

    it('Navigation to homepage by WH logo in the header', () => {
        Methods.navigateToFootballFeaturedPage(envData.device);
        cy.get(HOMEPAGE.PAGE_WRAPPER).should('not.exist');
        cy.get(HEADER.WH_LOGO).click({scrollBehavior: false});
        cy.get(HOMEPAGE.PAGE_WRAPPER).should('be.visible');

        switch (envData.device) {
            case 'DESKTOP':
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.not.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('not.be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('not.exist');
                cy.get(MOBILE_COMMON.BACK_BUTTON).should('not.exist');
                cy.get(MOB_MENU.SPORT_MENU).should('not.exist');
                break;
            case 'TABLET':
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('exist').and('not.be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                cy.get(MOB_MENU.SPORT_MENU).should('exist').and('not.be.visible');
                break;
            case 'MOBILE':
                cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('not.be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOB_MENU.SPORT_MENU).should('not.be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('not.exist');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('be.visible');
                cy.get(MOBILE_COMMON.BACK_BUTTON).should('not.exist');
                cy.get(MOB_MENU.HAMBURGER).should('be.visible');
                break;
        }
    })
});


