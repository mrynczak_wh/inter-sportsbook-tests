import * as SUB_HEADER from '../../../selectors/subHeader/sub-header';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

describe('HomePage', () => {
    beforeEach(() => {
        Methods.openHomePage();
    });

    ['FRACTIONAL', 'AMERICAN', 'DECIMAL'].forEach((format => {
        it(`Odds format change to ${format} - ${envData.jurisdiction} - ${envData.device}`, () => {
            switch(envData.device){
                case 'DESKTOP':
                    cy.get(SUB_HEADER.ODDS_FORMAT_BUTTON).click();
                    cy.get(SUB_HEADER.FORMAT_OPTION(format)).click();
                    Methods.checkOddsFormatIsCorrect(format);
                    break;
                case 'TABLET':
                    cy.get(SUB_HEADER.ODDS_FORMAT_TABLET_SETTINGS).click();
                    cy.get(SUB_HEADER.ODDS_FORMAT_BUTTON).click();
                    cy.get(SUB_HEADER.FORMAT_OPTION(format)).click();
                    Methods.checkOddsFormatIsCorrect(format);
                    break;
                case 'MOBILE':
                    cy.get(MOB_MENU.HAMBURGER).click();
                    cy.wait(500);
                    Methods.clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Formato de Cuota', 'Formato quota');
                    cy.get(MOB_MENU.FORMAT_OPTION(format)).eq(0).click();
                    Methods.checkOddsFormatIsCorrect(format);
                    break;
            }
        });
    }));
});

