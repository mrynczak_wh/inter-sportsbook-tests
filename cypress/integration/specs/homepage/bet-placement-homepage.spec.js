import * as Methods from '../../../support/methods';
import * as BETTING from '../../../selectors/betting/betting-options';
import * as HOMEPAGE from '../../../selectors/homepage/homepage';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

describe('HomePage', () => {

    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
    });


    it(`Bet placement in homepage - ${envData.jurisdiction} - ${envData.device}`, () => {
        switch(envData.device){
            case 'DESKTOP':
                cy.get(BETTING.HIGHLIGHTS_PREMATCH_SELECTION).eq(0).click({scrollBehavior: false, force: true});
                Methods.placeBetWithAssertion();
                break;
            case 'MOBILE':
            case 'TABLET':
                cy.get(HOMEPAGE.MOBILE_HIGHLIGHTS_TAB).click({scrollBehavior: false, force: true});
                cy.get(BETTING.HIGHLIGHTS_PREMATCH_SELECTION).eq(0).click({scrollBehavior: false});
                Methods.placeBetWithAssertion();
                break;
        }
    })
});

