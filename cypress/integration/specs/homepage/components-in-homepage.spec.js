import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as HOMEPAGE from '../../../selectors/homepage/homepage';
import * as BETSLIP from '../../../selectors/betslipWidget/betslip';
import * as SUB_HEADER from '../../../selectors/subHeader/sub-header';
import * as HEADER from '../../../selectors/productHeader/header';
import * as MOBILE_TOOLBAR from '../../../selectors/mobileBottomToolbar/toolbar';
import * as MOBILE_CAROUSEL from '../../../selectors/mobileSportsCarousel/sportsCarousel';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

describe('HomePage', () => {
    beforeEach(() => {
        Methods.openHomePage();
    });


    it(`Components in homepage - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(HOMEPAGE.PAGE_WRAPPER).should('be.visible');
        cy.get(HOMEPAGE.MAIN_CONTENT_AREA).should('be.visible');
        cy.get(HOMEPAGE.FOOTER_WRAPPER).should('be.visible');

        switch (envData.device) {
            case 'DESKTOP':
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(HOMEPAGE.RIGHT_COLUMN_AREA).should('be.visible');
                break;
            case 'TABLET':
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(HOMEPAGE.RIGHT_COLUMN_AREA).should('not.be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                break;
            case 'MOBILE':
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(HOMEPAGE.MOBILE_CATEGORY_TABS).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('be.visible');
                cy.get(MOB_MENU.HAMBURGER).should('be.visible');
                break;
        }
    });
});


