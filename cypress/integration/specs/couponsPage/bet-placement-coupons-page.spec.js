import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as Methods from '../../../support/methods';
import * as BETTING from '../../../selectors/betting/betting-options';
import * as COUPONS from "../../../selectors/couponsPage/coupons-page-selectors";
import '../../../support/index';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

describe('Coupons Page', () => {

    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
        Methods.navigateToFootballCouponsPage(envData.device);
    });


    it(`Bet placement in coupons page - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(COUPONS.COUPONS_PAGE_WRAPPER).should('be.visible');
        cy.get(COUPONS.SINGLE_COUPON).eq(0).click({force: true, scrollBehavior: false});
        cy.get(BETTING.SELECTION_ODDS).eq(0).click({force: true, scrollBehavior: false});
        Methods.placeBetWithAssertion();
    })
});

