import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';
import * as IN_PLAY_PAGE from '../../../selectors/inPlayPage/inPlayPage';
import * as EVENT_PAGE from '../../../selectors/eventPage/eventPage';
import * as BETTING from "../../../selectors/betting/betting-options";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('In Play Event Page', () => {
    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
        Methods.navigateToAllInPlayPage(envData.device);
    });


    it(`Placing bet in football daily list page - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(IN_PLAY_PAGE.IN_PLAY_PAGE_WRAPPER).should('be.visible');
        Methods.selectInPlayEvent();
        cy.get(EVENT_PAGE.EVENT_PAGE_WRAPPER).should('be.visible');
        cy.get(EVENT_PAGE.MARKETS_MENU).should('be.visible');
        cy.get(EVENT_PAGE.MARKETS_CONTAINER).should('be.visible');
        cy.get(BETTING.SELECTION_ODDS).eq(0).click({scrollBehavior: false});
        Methods.placeBetWithAssertion();
    })
});
