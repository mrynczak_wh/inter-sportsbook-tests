import * as DAILY_LIST from '../../../selectors/dailyListPage/dailyListPageSelectors';
import * as Methods from '../../../support/methods';
import * as EVENT_PAGE from '../../../selectors/eventPage/eventPage';
import * as BETTING from "../../../selectors/betting/betting-options";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('Event Page Football', () => {
    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
        Methods.navigateToFootballDailyListPage(envData.device);
    });


    it(`Placing bet in football prematch event page - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(DAILY_LIST.DAILY_LIST_WRAPPER).should('be.visible');
        Methods.selectPrematchEvent();
        cy.get(EVENT_PAGE.EVENT_PAGE_WRAPPER).should('be.visible');
        cy.get(EVENT_PAGE.MARKETS_MENU).should('be.visible');
        cy.get(EVENT_PAGE.MARKETS_CONTAINER).should('be.visible');
        cy.get(BETTING.SELECTION_ODDS).eq(0).click({scrollBehavior: false});
        Methods.placeBetWithAssertion();
    })
});

