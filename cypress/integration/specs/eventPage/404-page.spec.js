import * as ERROR_404_PAGE from '../../../selectors/404-page/404-page';
import * as Methods from "../../../support/methods";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

if(envData.jurisdiction !== 'ES') {
    describe('404 Page', () => {
        beforeEach(() => {
            cy.visit('betting/it-it/non-existing-sport-page', {failOnStatusCode: false});
        });


        it(`Test of elements of 404 page - ${envData.jurisdiction} - ${envData.device}`, () => {
            cy.get(ERROR_404_PAGE.ERROR_BUTTON).should('be.visible');
            cy.get(ERROR_404_PAGE.MESSAGE_BOX).should('be.visible');
            cy.get(ERROR_404_PAGE.FOOTER).should('be.visible');
            cy.get(ERROR_404_PAGE.HEADER).should('be.visible');
            cy.get(ERROR_404_PAGE.ERROR_MESSAGE).should('contain.html', ERROR_404_PAGE.ERROR_MESSAGE_CONTENT());

        })
    });
}


