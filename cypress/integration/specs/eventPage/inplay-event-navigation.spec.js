import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as TOP_BETS from '../../../selectors/topBetsWidget/top-bets-widget';
import * as BETSLIP from '../../../selectors/betslipWidget/betslip';
import * as SUB_HEADER from '../../../selectors/subHeader/sub-header';
import * as HEADER from '../../../selectors/productHeader/header';
import * as MOBILE_TOOLBAR from '../../../selectors/mobileBottomToolbar/toolbar';
import * as MOBILE_CAROUSEL from '../../../selectors/mobileSportsCarousel/sportsCarousel';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';
import * as IN_PLAY_PAGE from '../../../selectors/inPlayPage/inPlayPage';
import * as EVENT_PAGE from '../../../selectors/eventPage/eventPage';
import * as BREADCRUMBS from "../../../selectors/breadcrumbsPage/breadcrumbs";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('In Play Event Page', () => {
    beforeEach(() => {
        Methods.openHomePage();
        Methods.navigateToAllInPlayPage(envData.device)
    });


    it(`Navigation to football daily list page from sportsMenu - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(IN_PLAY_PAGE.IN_PLAY_PAGE_WRAPPER).should('be.visible');
        Methods.selectInPlayEvent();
        cy.get(EVENT_PAGE.EVENT_PAGE_WRAPPER).should('be.visible');
        cy.get(EVENT_PAGE.MARKETS_MENU).should('be.visible');
        cy.get(EVENT_PAGE.MARKETS_CONTAINER).should('be.visible');
        cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');
        cy.get(MOBILE_CAROUSEL.CAROUSEL).should('not.exist');

        switch(envData.device) {
            case 'DESKTOP':
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('not.be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                break;
            case 'TABLET':
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('be.visible');
                break;
            case 'MOBILE':
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('not.be.visible');
                cy.get(MOB_MENU.SPORT_MENU).should('not.be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('not.exist');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('exist').and('be.visible');
                break;
        }
    })
});

