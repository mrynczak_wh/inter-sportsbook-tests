import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as FEATURED_PAGE from '../../../selectors/featuredPage/featuredPage';
import * as TOP_BETS from '../../../selectors/topBetsWidget/top-bets-widget';
import * as BETSLIP from '../../../selectors/betslipWidget/betslip';
import * as SUB_HEADER from '../../../selectors/subHeader/sub-header';
import * as HEADER from '../../../selectors/productHeader/header';
import * as MOBILE_TOOLBAR from '../../../selectors/mobileBottomToolbar/toolbar';
import * as MOBILE_CAROUSEL from '../../../selectors/mobileSportsCarousel/sportsCarousel';
import * as MOB_MENU from '../../../selectors/sportsMenu/mobile-menu';
import * as Methods from '../../../support/methods';
import * as BREADCRUMBS from "../../../selectors/breadcrumbsPage/breadcrumbs";

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('Featured Page', () => {
    beforeEach(() => {
        Methods.openHomePage();
        Methods.navigateToFootballFeaturedPage(envData.device);
    });


    it(`Navigation to football featured page from sportsMenu - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(FEATURED_PAGE.PAGE_WRAPPER).should('be.visible');
        Methods.shouldHaveTextAccordingJusrisdiction(FEATURED_PAGE.PAGE_TITLE, 'Fútbol Populares', 'Calcio Popolari');
        cy.get(TOP_BETS.TOP_BETS_WIDGET).should('not.exist');

        switch(envData.device){
            case 'DESKTOP':
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('not.be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('not.be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('not.exist');
                break;
            case 'TABLET':
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('exist');
                cy.get(DSK_MENU.SPORTS_MENU).should('be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('be.visible');
                cy.get(HEADER.NAV_MENU).should('be.visible');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('not.be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('exist').and('be.visible');
                break;
            case 'MOBILE':
                cy.get(BETSLIP.BETSLIP_WRAPPER).should('not.be.visible');
                cy.get(MOB_MENU.SPORT_MENU).should('not.be.visible');
                cy.get(SUB_HEADER.SUB_HEADER).should('not.exist');
                cy.get(MOBILE_TOOLBAR.TOOLBAR_WRAPPER).should('be.visible');
                cy.get(MOBILE_CAROUSEL.CAROUSEL).should('be.visible');
                cy.get(BREADCRUMBS.BACK_BUTTON).should('exist').and('be.visible');
                break;
        }
    })
});
