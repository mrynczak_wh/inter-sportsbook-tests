import * as Methods from '../../../support/methods';
import * as BETTING from '../../../selectors/betting/betting-options';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

describe('Featured Page', () => {

    beforeEach(() => {
        Methods.openHomePage();
        Methods.loginToSportsbook();
        Methods.navigateToFootballFeaturedPage(envData.device);
    });


    it(`Bet placement in football featured page - ${envData.jurisdiction} - ${envData.device}`, () => {
        cy.get(BETTING.HIGHLIGHTS_PREMATCH_SELECTION).eq(0).click({force: true, scrollBehavior: false});
        Methods.placeBetWithAssertion();
    })
});

