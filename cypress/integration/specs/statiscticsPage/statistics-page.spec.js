import * as DSK_MENU from '../../../selectors/sportsMenu/desktop-menu';
import * as Methods from '../../../support/methods';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();


describe('Statistics page', () => {
    beforeEach(() => {
        Methods.openHomePage();
    });


    it(`Check if link button of statistics page has correct url - ${envData.jurisdiction} - ${envData.device}`, () => {
        Methods.navigateToFootballFeaturedPage(envData.device);
        Methods.checkStatisticsLink();
    })
});
