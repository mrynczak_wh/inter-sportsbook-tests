import * as Methods from '../../../support/methods';
import * as BYO from '../../../selectors/buildYourOdds/buildYourOdds';
import * as BETSLIP from '../../../selectors/betslipWidget/betslip';
import * as DAILY_LIST from '../../../selectors/dailyListPage/dailyListPageSelectors';

Cypress.on('uncaught:exception', () => {
    // returning false here prevents Cypress from failing the test
    return false
});

const envData = Methods.getEnvData();

const data = {
    PP1: {
        pds: 'https://ctgdata.williamhill-pp1.es/catalogs/OB_CG0/sports/OB_SP9/byoEventIds',
        singleEventData: 'https://sports.williamhill-pp1.es/data/boe01/es-es/event/',
        url: 'https://sports.williamhill-pp1.es/betting/es-es/f%C3%BAtbol/'
    },
    PROD: {
        pds: 'https://ctgdata.williamhill.es/catalogs/OB_CG0/sports/OB_SP9/byoEventIds',
        singleEventData: 'https://sports.williamhill.es/data/boe01/es-es/event/',
        url: 'https://sports.williamhill.es/betting/es-es/f%C3%BAtbol/'
    }
};

let eventsNumber = Number();
let url = data[envData.environment].url;
let marketsUrl = data[envData.environment].singleEventData;

const dictionary = {
    COMBO_ERROR_TEXT: 'Lo sentimos, tu última selección no puede combinarse',
    BETSLIP_SELECTION_TITLE: '1 Selección',
    INSTRUCTIONS: 'Elige entre dos y seis selecciones para comenzar a crear tu propia #MiApuesta.',
    CREATE_BUTTON_LABEL: 'Crear'
};

// TODO test scenarios - draft titles
// after selecting 6 selections the rest is desabled and user cannot add any more
// after clicking on view selections button all selections are visible
// clearing all selections
// after selecting only one selection an info message is displayed and betslip buttons are not visible

describe('Build Your Odds Tests', () => {

    before(() => {
        cy.request(data[envData.environment].pds).then((response) => {
            eventsNumber = response.body.length
            const eventIndex = Math.floor(Math.random() * eventsNumber);
            url += response.body[eventIndex];
            marketsUrl += `${response.body[eventIndex]}/markets`;
        });
    });

    beforeEach(() => {
        cy.visit(url);
        Methods.acceptCookiePolicy();
    });

    context('Authenticated user', () => {

        beforeEach(() => {
            Methods.loginToSportsbook();
            cy.get(BYO.BYO_LINK).click({ scrollBehavior: false, force: true });
            cy.get(BYO.MODAL_LAYOUT).should('exist').and('be.visible');
        });
        
        it('Placing a bet via BYO', () => {
            // TODO: randomize selections
            cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
            cy.get(BYO.BET_BUTTON).eq(10).click({ scrollBehavior: false, force: true });
            cy.get(BYO.SLIP_BUTTON).click({ scrollBehavior: false, force: true });
            cy.get(BYO.MODAL_LAYOUT).should('not.exist');
            cy.get(BETSLIP.SELECTION).should('exist');
            Methods.placeBetWithAssertion();
        });
        
        it('Combination error', () => {
            cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
            cy.get(BYO.BET_BUTTON).eq(1).click({ scrollBehavior: false, force: true });
            cy.get(BYO.COMBO_ERROR).should('exist').and('be.visible').and('have.text', dictionary.COMBO_ERROR_TEXT);
            cy.get(BYO.PRICE_ODDS).should('not.exist');
        });
        
        it('BYO window contains all of the markets from event data', () => {
            cy.request(marketsUrl).then((response) => {
                const marketsArrayFromResponse = response.body.byoMarketGroups.map(market => market.categoryName);
                for (let i = 0; i < marketsArrayFromResponse.length; i += 1) {
                    cy.get(BYO.MARKET_SECTION).eq(i).contains(marketsArrayFromResponse[i]);
                };
            });
        });

        it('After deselecting a selection disappears from BYO betslip', () => {
            cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
            cy.get(BYO.BETSLIP_CONTAINER).should('be.visible').contains(dictionary.BETSLIP_SELECTION_TITLE);
            cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
            cy.get(BYO.BETSLIP_CONTAINER).should('not.exist');
        });
        
        it('BYO selections are blocked when on selections overview', () => {
            cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
            cy.get(BYO.BET_BUTTON).eq(10).click({ scrollBehavior: false, force: true });
            cy.get(BYO.VIEW_SELECTIONS_BUTTON).click({ scrollBehavior: false });
            cy.get(BYO.CONTENT_WRAPPER).should('exist').and('have.class', BYO.CONTENT_WRAPPER_BLOCKED).click();
            cy.get(BYO.CONTENT_WRAPPER).should('exist').and('not.have.class', BYO.CONTENT_WRAPPER_BLOCKED);
        });
        
        it('Removing a single selection from BYO betslip', () => {
            cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
            cy.get(BYO.BET_BUTTON).eq(10).click({ scrollBehavior: false, force: true });
            
            cy.get(BYO.VIEW_SELECTIONS_BUTTON).click({ scrollBehavior: false });
            cy.get(BYO.SELECTIONS_COUNT).should('exist').and('have.text', '2');
            cy.get(BYO.BETSLIP_CONTAINER).within(() => {
                cy.get(BYO.BETLIST_ITEM).should('have.length', 2);
            });
            
            cy.get(BYO.BETLIST_DELETE_BUTTON).eq(0).click();

            cy.get(BYO.BETSLIP_CONTAINER).should('be.visible').contains(dictionary.BETSLIP_SELECTION_TITLE);
            cy.get(BETSLIP.SELECTION_TITLES_WRAPPER).children().should('have.length', 1);
        });
    });
    
    context('BYO on Daily List page', () => {
        
        beforeEach(() => {
            Methods.loginToSportsbook();
            Methods.navigateToFootballDailyListPage(envData.device);
            cy.get(DAILY_LIST.CONTENT).should('exist').and('be.visible');
            cy.contains(DAILY_LIST.MARKET_TYPE_SELECTOR, 'Mercado').click({ scrollBehavior: false });
            cy.contains(DAILY_LIST.MARKET_TYPE_SELECTION, 'Crear #MiApuesta').click({ scrollBehavior: false });
        });
        
        it('Check number of displayed events', () => {
            let displayedEvents = 0;
            cy.get(DAILY_LIST.DAY_FILTERS_CAROUSEL).find(DAILY_LIST.CAROUSEL_ITEM).each(element => {
                cy.wrap(element).find('a').click({ scrollBehavior: false, force: true });
                cy.scrollTo('bottom');
                cy.get(DAILY_LIST.EVENTS_SKELETON, { timeout: 20000 }).should('not.exist');
                cy.get(DAILY_LIST.CONTENT)
                    .should('exist')
                    .and('be.visible')
                    .find('article')
                    .then(elements => { displayedEvents += elements.length });
                    }).then(() => {
                        expect(displayedEvents).to.eq(eventsNumber)
                    });
        });
        
        it('Adding selections to betslip', () => {
            cy.get(BYO.INFO_MESSAGE).should('be.visible').contains(dictionary.INSTRUCTIONS);
            cy.getRandomElement(BYO.BET_BUTTON).contains(dictionary.CREATE_BUTTON_LABEL).click({ scrollBehavior: false, force: true });
            cy.get(BYO.MODAL_LAYOUT).should('exist').and('be.visible').within(() => {
                cy.get(BYO.BET_BUTTON).eq(0).click({ scrollBehavior: false, force: true });
                cy.get(BYO.BET_BUTTON).eq(10).click({ scrollBehavior: false, force: true });
            });
            cy.get(BETSLIP.SELECTION_TITLES_WRAPPER).children().should('have.length', 2);
            cy.get(BYO.SLIP_BUTTON).click({ scrollBehavior: false, force: true });
            cy.get(BYO.MODAL_LAYOUT).should('not.exist');
            cy.get(BETSLIP.SELECTION).should('exist');
        });
    });
    
    context('Unauthenticated user', () => {
        
        it('BYO opens after login', () => {
            cy.get(BYO.BYO_LINK).click({ scrollBehavior: 'top', force: true });
            Methods.fillLoginForm();
            cy.get(BYO.MODAL_LAYOUT).should('exist').and('be.visible');
        });
    });
});