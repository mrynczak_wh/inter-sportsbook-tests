export const PAGE_WRAPPER = '[data-page="home"]';
export const MOBILE_CATEGORY_TABS = 'nav.tabs__nav:not([role="tablist"])'; // to not include tabs from Desktop view
export const MAIN_CONTENT_AREA = '[class="mask -home"]';
export const RIGHT_COLUMN_AREA = 'div.off-canvas__right-wrapper';
export const FOOTER_WRAPPER = 'div#footer-root';
export const USABILLA_IFRAME = 'iframe[title="Usabilla Feedback Form"]';
export const USABILLA_OVERLAY = 'div.usabilla__overlay';
export const MOBILE_HIGHLIGHTS_TAB = '[data-name="Highlights"]';
