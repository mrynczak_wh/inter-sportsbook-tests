export const SUB_HEADER = 'div.sb-header-secondary-panel__content';
export const ODDS_FORMAT_BUTTON = '[data-test-id="@sportsbook/odds_odds-selector"]';
export const ODDS_FORMAT_TABLET_SETTINGS = 'div#settings';
export const FORMAT_OPTION = (format) => {
    switch (format) {
        case 'FRACTIONAL':
            return '[data-test-id="@sportsbook/odds.item_fraction"]';
        case 'AMERICAN':
            return '[data-test-id="@sportsbook/odds.item_american"]';
        case 'DECIMAL':
            return '[data-test-id="@sportsbook/odds.item_decimal"]';
    }
};
