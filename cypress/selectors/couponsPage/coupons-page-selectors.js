export const COUPONS_PAGE_WRAPPER = 'main.coupons';
export const COUPONS_ACCORDIONS = '#highlights-coupons';
export const PAGE_TITLE = 'span[class*="pageTitle__page-title"]';
export const COUPONS_PREVIEW = 'section.coupon-container';
export const SINGLE_COUPON = 'section.marketmenu__section a';
