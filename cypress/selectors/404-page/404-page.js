export const ERROR_BUTTON = 'a.error-button';
export const MESSAGE_BOX = 'div.messageBox';
export const FOOTER = 'div.footer-content';
export const HEADER = 'body header';
export const ERROR_MESSAGE = 'main h3';
export const ERROR_MESSAGE_CONTENT = () => {
    if (Cypress.env('jurisdiction') === 'it') {
        return '<span>\n' +
            '\t\t\t\t\tSembra che la pagina che stai cercando non esista.<br>\n' +
            '\t\t\t\t\tErrore 404.\n' +
            '\t\t\t\t</span>'
    }
    else {
        return '<span>Spanish text = currently 404 is broken - no text available</span>'
    }
};
