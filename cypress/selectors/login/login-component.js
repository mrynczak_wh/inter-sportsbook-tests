export const LOGIN_BUTTON = '[data-test-id="@sitebase/login-button_loginButtonId"]';
export const USERNAME_INPUT = 'input[name="username"]';
export const PASSWORD_INPUT = 'input[name="password"]';
export const LOGIN_SUBMIT = '[name="login-submit-button"]';
