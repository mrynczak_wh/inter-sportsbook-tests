export const SELECTION = 'button.sp-betbutton';
export const HIGHLIGHTS_PREMATCH_SELECTION = 'section[id*=match-highlights] button.betbutton';
export const SELECTION_ODDS = 'span.betbutton__odds';
export const PREMATCH_EVENT = 'div.sp-o-market__title a';
export const INPLAY_EVENT = 'div.event li a[title]';
export const BET_BUTTON_ODDS = '.btn.betbutton.oddsbutton'
