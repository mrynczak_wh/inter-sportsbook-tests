export const SPORTS_MENU = '#desktop-sidebar';
export const SPORTS_ITEM = (sport) => `[data-id-en="nav-${sport}"]`;
export const DAILY_LIST = '[data-id-en="nav-football-matches"]';
export const COMPETITIONS = '[data-id-en="nav-football-competitions"]';
export const STATISTICS_LINK = '[data-id-en="nav-football-stats"] a';
export const COUPONS = '[data-id-en="nav-football-coupons"]';
export const IN_PLAY = '[data-id-en="nav-inPlay"]';
export const HORSE_RACING = '[data-id-en="nav-horse-racing"]'
