export const SPORT_MENU = '.sidebar-navigation-wrapper';
export const HAMBURGER = '[data-test-id="layout.hamburger"]';
export const MENU_ITEM = 'span.sidebar-navigation__name';
export const FORMAT_OPTION = (format) => {
    switch (format) {
        case 'FRACTIONAL':
            return 'span.icon-fraction';
        case 'AMERICAN':
            return 'span.icon-american';
        case 'DECIMAL':
            return 'span.icon-decimal';
    }
};
