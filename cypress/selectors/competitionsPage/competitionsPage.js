export const COMPETITIONS_PAGE_WRAPPER = '[class="maincenter competitions"]';
export const COMPETITIONS_LIST = '[data-test-id="competitions-menu"]';
export const PAGE_TITLE = '[data-test-id="header-title"]';
export const SINGLE_COMP = '[data-test-id="GridItem.root"]';
