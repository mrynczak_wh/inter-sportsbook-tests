export const EVENT_PAGE_WRAPPER = 'main.event';
export const MARKETS_MENU = 'div#marketCollectionsMenu';
export const MARKETS_CONTAINER = 'div#eventEntity';
export const EVENT_ERROR_MESSAGE = '.event-page-error';
