//DESKTOP
export const BETSLIP_WRAPPER = '[data-test-id="betslip-container-ui"]';
export const STAKE_INPUT = '[class="bs-bet-stake__value"] input';
export const PLACE_BET_BUTTON = '.bs-bet-place';
export const BET_PLACED_HEADER = 'header.bs-receipt-header__header';
export const INFO_LABEL = 'bs-bet-label--info';
export const WARNING_MESSAGE = 'bs-bet-message--warning';
export const SELECTION = '.bs-selection';
export const SELECTION_TITLES_WRAPPER = '.byob-selection-view-title__subtitle';

//MOBILE
export const STAKE_INPUT_MOB = 'span.bs-bet-stake__value-wrapper--empty';
export const KEYPAD_BUTTON_MOB = 'button.bs-keypad-button';
