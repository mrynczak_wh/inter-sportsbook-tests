export const HOME_BUTTON_DESKTOP = 'div#breadcrumbsV2-app nav div:nth-child(2)';

//MOBILE
export const BACK_BUTTON = '[data-test-id="Breadcrumb.button-return"]';
export const HOME_BUTTON_MOBILE = 'div#breadcrumbsV2-app nav>div>div>span:nth-child(1)';
