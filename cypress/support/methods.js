import * as LOGIN from '../selectors/login/login-component';
import {esLoginData, itLoginData} from '../fixtures/login-data';
import * as BETSLIP from '../selectors/betslipWidget/betslip';
import * as MY_ACCOUNT from '../selectors/myAccount/myAccount';
import * as COOKIE_BANNER from '../selectors/cookiePolicyBanner/cookieBanner';
import * as ONBOARDING_MODAL from '../selectors/onboardingModal/onBoardingModal';
import * as BETTING from "../selectors/betting/betting-options";
import * as DSK_MENU from "../selectors/sportsMenu/desktop-menu";
import * as MOB_MENU from '../selectors/sportsMenu/mobile-menu';
import * as HOMEPAGE from "../selectors/homepage/homepage";
import * as MOBILE_CAROUSEL from "../selectors/mobileSportsCarousel/sportsCarousel";

export const getEnvData = () => {
    const parameters = ['jurisdiction', 'device', 'environment'];
    const envData = {}
    parameters.forEach(parameter => {
        envData[parameter] = Cypress.env(parameter).toUpperCase();
    });
    return envData
}

export const shouldHaveTextAccordingJusrisdiction = (element, esValue, itValue) => {
    cy.get(element).should('have.text', (Cypress.env('jurisdiction') === 'es') ?
        esValue :
        itValue);
};

export const clickByTextAccordingJurisdiction = (element, esValue, itValue) => {
    cy.get(element).contains((Cypress.env('jurisdiction') === 'es') ?
        esValue :
        itValue)
        .click({scrollBehavior: false, force: true});
};

export const putStakeInBetslip = () => {
    if (Cypress.env('device') === 'desktop') {
        cy.get(BETSLIP.STAKE_INPUT, {timeout: 20000}).should('exist');
        cy.get(BETSLIP.STAKE_INPUT).type((Cypress.env('jurisdiction') === 'es') ? '0.06' : '2', {force: true});
    }
    else {
        cy.get(BETSLIP.STAKE_INPUT_MOB, {timeout: 20000}).should('exist');
        cy.get(BETSLIP.STAKE_INPUT_MOB).click();
        if(Cypress.env('jurisdiction') === 'es') {
            cy.get(BETSLIP.KEYPAD_BUTTON_MOB).contains(',').click();
            cy.get(BETSLIP.KEYPAD_BUTTON_MOB).contains('0').click();
            cy.get(BETSLIP.KEYPAD_BUTTON_MOB).contains('6').click();
        }
        if(Cypress.env('jurisdiction') === 'it') {
            cy.get(BETSLIP.KEYPAD_BUTTON_MOB).contains('2').click();
        }
    }
};

export const loginToSportsbook = () => {
    const dataPerJuris = Cypress.env('jurisdiction') === 'es' ? esLoginData: itLoginData;
    const dataPerEnv = Cypress.env('environment') === 'prod' ? dataPerJuris.prod : dataPerJuris.pp1;
    cy.get(LOGIN.LOGIN_BUTTON).click();
    cy.get(LOGIN.USERNAME_INPUT).type(dataPerEnv.user);
    cy.get(LOGIN.PASSWORD_INPUT).type(dataPerEnv.password);
    cy.get(LOGIN.LOGIN_SUBMIT).click();
    cy.get(MY_ACCOUNT.BALANCE_BUTTON_LOGGED_IN, {timeout: 10000});
    closeOnBoardingModal();
};

export const fillLoginForm = () => {
    const dataPerJuris = Cypress.env('jurisdiction') === 'es' ? esLoginData: itLoginData;
    const dataPerEnv = Cypress.env('environment') === 'prod' ? dataPerJuris.prod : dataPerJuris.pp1;
    cy.get(LOGIN.USERNAME_INPUT).type(dataPerEnv.user);
    cy.get(LOGIN.PASSWORD_INPUT).type(dataPerEnv.password);
    cy.get(LOGIN.LOGIN_SUBMIT).click();
}

export const logoutInSportsbook = () => {
    cy.get(MY_ACCOUNT.BALANCE_BUTTON_LOGGED_IN).click();
    cy.get(MY_ACCOUNT.LOGOUT_BUTTON).click();
};

export const placeBet = () => {
    cy.get(BETSLIP.PLACE_BET_BUTTON).should('not.be.disabled');
    cy.get(BETSLIP.PLACE_BET_BUTTON).click({scrollBehavior: false, force: true});
};

export const assertBetIsPlaced = () => {
    cy.get(BETSLIP.BET_PLACED_HEADER, { timeout: 25000 }).should('contain', Cypress.env('jurisdiction') === 'es' ? 'Apuesta realizada' : 'Scommessa Piazzata');
};

export const acceptCookiePolicy = () => {
    cy.get(COOKIE_BANNER.ACCEPT_BUTTON).click();
};

export const closeOnBoardingModal = () => {
    cy.get("body").then($body => {
        if ($body.find(ONBOARDING_MODAL.CLOSE_BUTTON).length > 0) {
            cy.get(ONBOARDING_MODAL.CLOSE_BUTTON).click({force: true});
        }
    });
};

export const checkLangOfHomepage = () => {
    const esLang = 'es-es';
    const itLang = 'it-it';
    cy.url().should('include', Cypress.env('jurisdiction') === 'es' ? esLang : itLang);
    cy.location().should(loc => {
        expect(loc.pathname).to.eq(Cypress.env('jurisdiction') === 'es' ? `/betting/${esLang}` : `/betting/${itLang}`);
    })
};

export const checkOddsFormatIsCorrect = (format) => {
    const expectedFormat = (odds_format) => {
        switch (odds_format) {
            case 'FRACTIONAL':
                return new RegExp('\\d+[/]\\d+|EVS|SP');
            case 'AMERICAN':
                return new RegExp('[+-]\\d+');
            case 'DECIMAL':
                return new RegExp('\\d+[.]\\d+');
        }
    };

    cy.get(BETTING.SELECTION_ODDS).each($odd => {
        cy.wrap($odd.text()).should('match', expectedFormat(format));
    })
};

export const checkStatisticsLink = () => {
    cy.get(DSK_MENU.STATISTICS_LINK).should('have.attr', 'href',
        Cypress.env('jurisdiction') === 'es' ? 'https://whill.touch-line.com/?lang=7' : 'https://whill.touch-line.com/?lang=10');
};

export const getRidOfUsabillaFeedBack = () => {
    if(Cypress.env('jurisdiction') === 'es') {
        cy.get(HOMEPAGE.USABILLA_IFRAME);
        cy.window(). then(window => window.document.querySelector(HOMEPAGE.USABILLA_OVERLAY).parentNode.removeChild(Cypress.$(HOMEPAGE.USABILLA_OVERLAY)[0]));
    }
};

export const openHomePage = () => {
    cy.visit('/');
    // getRidOfUsabillaFeedBack();
    acceptCookiePolicy();
};

export const navigateToAllInPlayPage = (device) => {

    switch (device.toUpperCase()) {
      case 'MOBILE':
        cy.get(MOB_MENU.HAMBURGER).click();
        cy.wait(500);
        clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'En Directo', 'Live');
        clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Eventos en Directo', 'Eventi Live');
        break;
      case 'DESKTOP':
      case 'TABLET':
        cy.get(DSK_MENU.IN_PLAY).click({scrollBehavior: false});
        break;
      default:
        throw new Error(`Error: Bad device name: ${device}.`)
    }
};

export const navigateToHorseRacingPage = device => {

    switch (device.toUpperCase()) {
        case 'MOBILE':
            cy.get(MOB_MENU.HAMBURGER).click()
            cy.wait(500);
            cy.get(MOB_MENU.MENU_ITEM).contains('Deportes A-Z').click();
            cy.get(MOB_MENU.MENU_ITEM).contains('Caballos').click();
            break;
        case 'TABLET':
        case 'DESKTOP':
            cy.get(DSK_MENU.HORSE_RACING).click({scrollBehavior: false});
            break;
      default:
            throw new Error(`Error: Bad device name: ${device}.`)
    }
};

export const selectPrematchEvent = () => {
    cy.get(BETTING.PREMATCH_EVENT).eq(0).click({force: true, scrollBehavior: false});
};

export const selectInPlayEvent = () => {
    cy.get(BETTING.INPLAY_EVENT).eq(0).click({force: true, scrollBehavior: false});
};

export const navigateToFootballCompetitionsPage = device => {
    switch (device) {
        case 'MOBILE':
            cy.get(MOB_MENU.HAMBURGER).click();
            cy.wait(500);
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Deportes A-Z', 'Sport A-Z');
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Fútbol', 'Calcio');
            cy.get(MOBILE_CAROUSEL.COMPETITIONS).click();
            break;
        case 'DESKTOP':
        case 'TABLET':
            cy.get(DSK_MENU.SPORTS_ITEM('football')).eq(0).click({scrollBehavior: false});
            cy.get(DSK_MENU.COMPETITIONS).click({scrollBehavior: false});
            break;
    }
};

export const navigateToFootballCouponsPage = device => {
    switch (device) {
        case 'MOBILE':
            cy.get(MOB_MENU.HAMBURGER).click();
            cy.wait(500);
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Deportes A-Z', 'Sport A-Z');
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Fútbol', 'Calcio');
            cy.get(MOBILE_CAROUSEL.COUPONS).click();
            break;
        case 'DESKTOP':
        case 'TABLET':
            cy.get(DSK_MENU.SPORTS_ITEM('football')).eq(0).click({scrollBehavior: false});
            cy.get(DSK_MENU.COUPONS).click({scrollBehavior: false});
            break;
    }
};

export const navigateToFootballDailyListPage = device => {
    switch (device) {
        case 'MOBILE':
            cy.get(MOB_MENU.HAMBURGER).click();
            cy.wait(500);
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Deportes A-Z', 'Sport A-Z');
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Fútbol', 'Calcio');
            cy.get(MOBILE_CAROUSEL.DAILY_LIST).click();
            break;
        case 'DESKTOP':
        case 'TABLET':
            cy.get(DSK_MENU.SPORTS_ITEM('football')).eq(0).click({scrollBehavior: false});
            cy.get(DSK_MENU.DAILY_LIST).click({scrollBehavior: false});
            break;
    }
};

export const navigateToFootballFeaturedPage = device => {
    switch (device) {
        case 'MOBILE':
            cy.get(MOB_MENU.HAMBURGER).click();
            cy.wait(500);
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Deportes A-Z', 'Sport A-Z');
            clickByTextAccordingJurisdiction(MOB_MENU.MENU_ITEM, 'Fútbol', 'Calcio');
            break;
        case 'DESKTOP':
        case 'TABLET':
            cy.get(DSK_MENU.SPORTS_ITEM('football')).eq(0).click({scrollBehavior: false});
            break;
    }
};

export const placeBetWithAssertion = () => {
    putStakeInBetslip();
    cy.get(BETSLIP.INFO_LABEL).should('not.exist');
    cy.get(BETSLIP.WARNING_MESSAGE).should('not.exist');
    placeBet();
    assertBetIsPlaced();
};
