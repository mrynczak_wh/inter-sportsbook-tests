declare namespace Cypress {
    interface Chainable<Subject> {

        /**
         * Get an iframe element
         * @param selector
         */
        getIframe(selector: string): Chainable<Subject>;
    }
}
